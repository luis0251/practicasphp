<?php
$mensaje = "Hola Mundo Soy Luis";

//para saber la longitud de un String
echo strlen($mensaje);

echo "<br>";
//saber el numero de palabras
echo str_word_count($mensaje);

echo "<br>";
//Encontrar posicion de un texto
echo strpos($mensaje,"Mundo");
echo "<br>";
//Reemplazar texto
echo str_replace("Mundo" , " " , $mensaje );
echo "<br>";
//convertir a mayuscula
echo strtoupper($mensaje);
echo "<br>";
//minuscula
echo strtolower($mensaje);
echo "<br>";
// comparar cadenas , espacio en binario
echo strcmp("c","a");
echo "<br>";
//substraer cadenas
echo substr($mensaje,10);

//para ayudar con los espacios en blanco
echo "<br>";
echo trim("      Deja       los         espacios                    ");
?> 